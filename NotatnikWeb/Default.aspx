﻿<%@ Page Title="Notatnik" Language="C#" MasterPageFile="~/ProjektMaster.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NotatnikWeb.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <div class="col-md-6">
        <asp:GridView ID="gvNotatki" runat="server" DataSourceID="odsNotatki" EnableModelValidation="True" AutoGenerateColumns="False" CssClass="table" DataKeyNames="ID" GridLines="None">
            <Columns>
                <asp:BoundField DataField="Tekst" HeaderText="Tekst" SortExpression="Tekst" />
            </Columns>
            <EmptyDataTemplate>
                Brak notatek.
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
    <div class="col-md-6">

        <asp:TextBox ID="tbTekst" runat="server" TextMode="MultiLine" Width="100%" Rows="8"></asp:TextBox>
        <div style="text-align: right;">
            <asp:Button ID="btnDodaj" runat="server" Text="Dodaj" CssClass="btn btn-success" OnClick="btnDodaj_Click" />
        </div>
    </div>

    <asp:ObjectDataSource ID="odsNotatki" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="Wszystkie" TypeName="NotatnikWeb.Logika.NotatkaDO"></asp:ObjectDataSource>
</asp:Content>
