﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NotatnikWeb.Logika;

namespace NotatnikWeb
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnDodaj_Click(object sender, EventArgs e)
        {
            var n = Factory.CreateNotatka();
            var notatka = n.Nowa();
            notatka.Tekst = tbTekst.Text;
            n.Dodaj(notatka);
            Response.Redirect(Request.RawUrl);
        }
    }
}