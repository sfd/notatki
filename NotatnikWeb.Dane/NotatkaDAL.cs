﻿using System.Collections.Generic;
using System.Linq;
using Dapper;   // https://github.com/StackExchange/dapper-dot-net
using NotatnikWeb.Common;


namespace NotatnikWeb.Dane
{
    public class NotatkaDAL : INotatkaDAL
    {
        public IEnumerable<INotatkaItem> Wszystkie()
        {
            using (var conn = DBInfo.GetConnection())
            {
                return conn.Query<Notatka>("select * from notatka", null).Select(k => (INotatkaItem) k);
            }
        }

        public void Dodaj(INotatkaItem notatka)
        {
            using (var conn = DBInfo.GetConnection())
            {
                conn.Execute("insert into notatka(Tekst) values(@tekst)", notatka);
            }
        }

        public INotatkaItem Nowa()
        {
            return new Notatka();
        }
    }
}