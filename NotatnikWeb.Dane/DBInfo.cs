﻿using System.Data;
using MySql.Data.MySqlClient;

namespace NotatnikWeb.Dane
{
    public class DBInfo
    {
        public static string ConnectionString { get; set; }

        public static IDbConnection GetConnection()
        {
            return GetConnection(ConnectionString);
        }

        public static IDbConnection GetConnection(string connString)
        {
            MySqlConnection connection = new MySqlConnection(connString);
            connection.Open();
            return connection;
        } 
    }
}