﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NotatnikWeb.Common;

namespace NotatnikWeb.Dane
{
    public class Notatka : INotatkaItem
    {
        public int ID { get; set; }

        public string Tekst { get; set; }
    }
}
