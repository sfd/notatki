﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using NotatnikWeb.Common;
using NotatnikWeb.Dane;

namespace NotatnikWeb.Logika
{
    public class Notatka
    {
        private readonly INotatkaDAL notatkaDal = null;

        public INotatkaItem Nowa()
        {
            return notatkaDal.Nowa();
        }

        public Notatka(INotatkaDAL notatkaDal)
        {
            this.notatkaDal = notatkaDal;
        }

        public IEnumerable<INotatkaItem> Wszystkie()
        {
            return notatkaDal.Wszystkie();
        }

        public void Dodaj(INotatkaItem notatka)
        {
            notatkaDal.Dodaj(notatka);
        }
    }

    [DataObject]
    public static class NotatkaDO
    {
        private static readonly Notatka notatka = Factory.CreateNotatka();

        public static IEnumerable<INotatkaItem> Wszystkie()
        {
            return notatka.Wszystkie();
        }
    }
}
