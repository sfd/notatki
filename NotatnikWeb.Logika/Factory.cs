﻿using NotatnikWeb.Dane;

namespace NotatnikWeb.Logika
{
    public static class Factory
    {
        /// <summary>
        /// Metoda konfiguracji dostępu do bazy danych
        /// </summary>
        /// <param name="connectionString"></param>
        public static void SetConnectionString(string connectionString)
        {
            DBInfo.ConnectionString = connectionString;
        }

        /// <summary>
        /// Metoda tworząca obiekt logiki Notatka
        /// </summary>
        /// <returns></returns>
        public static Notatka CreateNotatka()
        {
            return new Notatka(new NotatkaDAL());
        }
    }
}