# README #

Krótki opis projektu aplikacji notatek

### Struktura ###

* NotatnikWeb - projekt w ASP .NET Web Forms
* NotatnikWeb.Common - wspólne definicje danych używanych w projekcie
* NotatnikWeb.Dane - warstwa dostępu do bazy danych
* NotatnikWeb.Logika - warstwa logiki projektu

### Uwagi ###

Projekt korzysta z bazy danych MySql i w warstwie danych wykorzystany jest odpowiedni sterownik ADO. Jako ORM jest wykorzystany Dapper (https://github.com/StackExchange/dapper-dot-net)