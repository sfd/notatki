﻿namespace NotatnikWeb.Common
{
    public interface INotatkaItem
    {
        /// <summary>
        /// Identyfikator
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// Treść notatki
        /// </summary>
        string Tekst { get; set; }
    }
}