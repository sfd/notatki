using System.Collections.Generic;

namespace NotatnikWeb.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface INotatkaDAL
    {
        /// <summary>
        /// Metoda pobieraj�ca wszystkie notatki
        /// </summary>
        /// <returns>Kolekcja notatek</returns>
        IEnumerable<INotatkaItem> Wszystkie();

        /// <summary>
        /// Metoda dodaj�ca notatk� do bazy danych
        /// </summary>
        /// <param name="notatka">Obiekt notatki</param>
        void Dodaj(INotatkaItem notatka);

        /// <summary>
        /// Metoda zwraca nowy obiekt notatki
        /// </summary>
        /// <returns></returns>
        INotatkaItem Nowa();
    }
}